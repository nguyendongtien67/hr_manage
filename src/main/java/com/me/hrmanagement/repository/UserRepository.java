package com.me.hrmanagement.repository;

import com.me.hrmanagement.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Boolean existsByUsernameContains(String username);
    User findUserByUsername(String username);
    User findUserById(int id);
}
