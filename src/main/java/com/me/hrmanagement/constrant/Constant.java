package com.me.hrmanagement.constrant;

public class Constant {
    public class CRUDState
    {
        public static final int SUCCESS = 1;
        public static final int ERROR = 2;
        public static final int DUPLICATE_ENTRY = 3;
    }

    public class Role
    {
        public static final String ADMIN = "admin";
        public static final String USER = "user";
        public static final String GUEST = "guest";
    }
}
