package com.me.hrmanagement.service.Impl;

import com.me.hrmanagement.DTO.UserDTO;
import com.me.hrmanagement.RequestObjects.UserRequest;
import com.me.hrmanagement.constrant.Constant;
import com.me.hrmanagement.entity.Role;
import com.me.hrmanagement.entity.User;
import com.me.hrmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.me.hrmanagement.repository.UserRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    @Lazy
    private PasswordEncoder passwordEncoder;

    @Override
    public ArrayList<UserDTO> getAllUser() {
        ArrayList<UserDTO> users = new ArrayList<>();

        for(User u: userRepository.findAll()) {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(u.getId());
            userDTO.setUsername(u.getUsername());


            users.add(userDTO);
        }
        return users;
    }

    /* Not finished method */
    @Override
    public int createUser(UserRequest userRequest) {
        String username = userRequest.getUsername();
        String password = userRequest.getPassword();
        int roleId = userRequest.getRoleId();

        //Set username, password and user role
        User u = new User();
        u.setUsername(username);
        u.setPassword(passwordEncoder.encode(password));

        Role role = new Role();
        role.setId(roleId);
        u.setRole(role);
        // Save to database
        try {
            //check duplicate username
            if(!userRepository.existsByUsernameContains(username)) {
                userRepository.save(u); // If username not duplicate, insert to DB
                return Constant.CRUDState.SUCCESS; //Success
            }
            return Constant.CRUDState.DUPLICATE_ENTRY; //Username exist on DB.
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
            return Constant.CRUDState.ERROR; //Exception
        }
    }

    //Spring security
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User u = userRepository.findUserByUsername(username);
        if(u == null)
        {
            throw new UsernameNotFoundException("User not found!");
        }
        Set<GrantedAuthority> grantedAuthoritySet = new HashSet<>();
        Role role = u.getRole();
        grantedAuthoritySet.add(new SimpleGrantedAuthority(role.getName()));
        return new org.springframework.security.core.userdetails.User(u.getUsername(), u.getPassword(), grantedAuthoritySet);
       // return new CustomUserDetails(u);
    }
}
