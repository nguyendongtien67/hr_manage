package com.me.hrmanagement.service;

import com.me.hrmanagement.DTO.UserDTO;
import com.me.hrmanagement.RequestObjects.UserRequest;
import com.me.hrmanagement.entity.Role;

import java.util.List;

public interface UserService {
    List<UserDTO> getAllUser();
    int createUser(UserRequest userRequest);
}
