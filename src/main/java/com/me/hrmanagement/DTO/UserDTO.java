package com.me.hrmanagement.DTO;

import com.me.hrmanagement.entity.Employee;
import com.me.hrmanagement.entity.Role;
import com.me.hrmanagement.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserDTO {
    private int id;
    private String username;
    private int roleId;
    private int employeeId;
    private String roleName;
    private String employeeName;

//    public UserDTO(User u) {
//        this.id = u.getId();
//        this.username = u.getUsername();
//        this.roleId = u.getRole().getId();
//        this.roleName = u.getRole().getName();
//        this.employeeId = u.getEmployee().getId();
//        this.employeeName = u.getEmployee().getName();
//    }
}
