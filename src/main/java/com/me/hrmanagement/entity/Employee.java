package com.me.hrmanagement.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="emp_name")
    private String name;

    @Column(name="emp_dob")
    private Date dob;

    @Column(name="emp_email")
    private String email;

    @Column(name = "emp_avatar")
    private String avatar;

    @ManyToOne
    @JoinColumn(name = "department_id", referencedColumnName = "id")
    private Department department;

    @OneToOne(mappedBy = "employee")
    private User user;
}
