package com.me.hrmanagement.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.me.hrmanagement.DTO.UserDTO;
import com.me.hrmanagement.RequestObjects.UserRequest;
import com.me.hrmanagement.Utils.ResponseUtils;
import com.me.hrmanagement.constrant.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.me.hrmanagement.service.UserService;

import java.util.List;

@RestController
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user")
    @JsonIgnore
    public List<UserDTO> getAllUser()
    {
        return userService.getAllUser();
    }

    // API endpoint to create a new user
    @PostMapping ("/user/create")
    public ResponseEntity<Object> createUser(@RequestBody UserRequest userRequest)
    {
        switch (userService.createUser(userRequest)){
            case Constant.CRUDState.SUCCESS:
                log.info("Created new user successfully");
                return ResponseUtils.generateResponse("Created new user successfully!", HttpStatus.OK, null);
            case Constant.CRUDState.DUPLICATE_ENTRY:
                log.info("Username exist case");
                return ResponseUtils.generateResponse("Username exist! Please use another name ", HttpStatus.INTERNAL_SERVER_ERROR, null);
            case Constant.CRUDState.ERROR:
                log.error("Created new user failed!");
                return ResponseUtils.generateResponse("Created new user failed!", HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
        return ResponseUtils.generateResponse("Created new user failed!", HttpStatus.INTERNAL_SERVER_ERROR, null);
    }

}
