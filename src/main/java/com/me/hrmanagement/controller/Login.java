package com.me.hrmanagement.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.me.hrmanagement.Utils.JsonUtils;
import com.me.hrmanagement.Utils.JwtTokenUtil;
import com.me.hrmanagement.Utils.ResponseUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RestController
@Slf4j
public class Login {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @PostMapping("/login")
    public String authenticateUser(@RequestBody String loginJson)
    {
        JsonObject jsonObject = JsonUtils.parseJson(loginJson);
        String username = jsonObject.get("username").getAsString();
        String password = jsonObject.get("password").getAsString();

        /* create Json response object */
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonObject responseObj = new JsonObject();
        try {
            // Kiểm tra đăng nhập. Nếu user login thành công thì sẽ không có exception.
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            responseObj.addProperty("token", jwtTokenUtil.generateToken(userDetails));
            return gson.toJson(responseObj);
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
            return e.getMessage();
        }
    }
}
