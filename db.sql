-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.24-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table mydb.department: ~1 rows (approximately)
INSERT INTO `department` (`id`, `name`) VALUES
	(1, 'HR');

-- Dumping data for table mydb.employee: ~0 rows (approximately)
INSERT INTO `employee` (`id`, `emp_name`, `emp_dob`, `emp_email`, `emp_avatar`, `department_id`) VALUES
	(3, 'Nguyễn Khải Hoàn', '2022-09-08 10:30:36', 'hoan489@gmail.com', NULL, 1);

-- Dumping data for table mydb.function: ~0 rows (approximately)

-- Dumping data for table mydb.role: ~2 rows (approximately)
INSERT INTO `role` (`id`, `name`) VALUES
	(1, 'admin'),
	(2, 'user'),
	(3, 'guest');

-- Dumping data for table mydb.role_functions: ~0 rows (approximately)

-- Dumping data for table mydb.user: ~1 rows (approximately)
INSERT INTO `user` (`id`, `username`, `password`, `role_id`, `employee_id`) VALUES
	(18, 'admin', 'memay1212', 2, NULL),
	(19, 'admin1', 'memay12121', 1, NULL);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
